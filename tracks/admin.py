from django.contrib import admin
from tracks.models import Track


@admin.register(Track)
class Track(admin.ModelAdmin):
    list_display = (
        "name",
        "track_number",
        "vinyl",
    )
