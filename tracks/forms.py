from django.forms import ModelForm
from tracks.models import Track

class TrackForm(ModelForm):
    class Meta:
        model = Track
        fields = (
            "name",
            "track_number",
            "vinyl",
            "assignee",
        )
