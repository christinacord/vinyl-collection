from django.db import models
from vinyls.models import Vinyl
from django.conf import settings


class Track(models.Model):
    name = models.CharField(max_length=150)
    track_number = models.IntegerField()
    vinyl = models.ForeignKey(
        Vinyl, related_name="tracks", on_delete=models.CASCADE
        )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tracks",
        null=True,
        on_delete=models.CASCADE,
    )
