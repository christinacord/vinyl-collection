from django.urls import path
from tracks.views import show_my_tracks_list, create_track

urlpatterns = [
    path("create/", create_track, name="create_track"),
    path("mine/", show_my_tracks_list, name="show_my_tracks_list"),
]
