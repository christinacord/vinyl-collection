from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tracks.forms import TrackForm
from tracks.models import Track


@login_required
def create_track(request):
    if request.method == "POST":
        form = TrackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = TrackForm()

    context = {
        "form": form,
    }
    return render(request, "tracks/create.html", context)


@login_required
def show_my_tracks_list(request):
    tracks = Track.objects.filter(assignee=request.user)
    context = {
        "tracks": tracks,
    }
    return render(request, "tracks/list.html", context)
