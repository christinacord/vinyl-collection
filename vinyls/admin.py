from django.contrib import admin
from vinyls.models import Vinyl, Account, Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Vinyl)
class VinylAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "artist",
    )
