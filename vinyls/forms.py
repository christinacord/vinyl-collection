from django.forms import ModelForm
from vinyls.models import Vinyl


class CreateVinylForm(ModelForm):
    class Meta:
        model = Vinyl
        fields = [
            "name",
            "artist",
            "owner",
            "category",
        ]
