from django.db import models
from django.conf import settings


class Category(models.Model):
    name = models.CharField(max_length=150)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
        )

    def __str__(self):
        return self.name


class Vinyl(models.Model):
    name = models.CharField(max_length=200)
    artist = models.CharField(max_length=150)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="vinyls",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        Category, related_name="vinyls", on_delete=models.CASCADE, null=True, blank=True
    )

    def __str__(self):
        return self.name
