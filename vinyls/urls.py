from django.urls import path
from vinyls.views import vinyls_list, vinyl_detail, add_vinyl


urlpatterns = [
    path("", vinyls_list, name="home"),
    path("<int:id>/", vinyl_detail, name="show_vinyl"),
    path("create/", add_vinyl, name="create_vinyl"),
]
