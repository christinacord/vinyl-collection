from django.shortcuts import render, redirect
from vinyls.models import Vinyl, Category
from django.contrib.auth.decorators import login_required
from vinyls.forms import CreateVinylForm

@login_required
def vinyls_list(request):
    vinyls = Vinyl.objects.filter(owner=request.user)
    context = {
        "vinyls": vinyls,
    }
    return render(request, "vinyls/list.html", context)


def vinyl_detail(request, id):
    vinyl = Vinyl.objects.get(id=id)
    details = Vinyl.objects.get(id=id).tracks.all()
    context = {
        "vinyl": vinyl,
        "details": details,
    }
    return render(request, "vinyls/detail.html", context)


def add_vinyl(request):
    if request.method == "POST":
        form = CreateVinylForm(request.POST)
        if form.is_valid():
            vinyl = form.save(False)
            vinyl.owner = request.user
            form.save()
            return redirect("home")
    else:
        form = CreateVinylForm()

    context = {
        "form": form,
    }
    return render(request, "vinyls/create.html", context)


def add_category(request):
    pass


def category_list(request):
    categories = Category.objects.all()
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)
